window.onload = function(){
    document.body.innerHTML = document.body.innerHTML + '<div class="topfix"> RABBIT </div>';
};

function myFunction(){

    // search text element
    let input = document.getElementById("mySearch");
    let filter = input.value.toUpperCase();
    let ul =document.getElementById("myUL");
    let li = ul.getElementsByTagName("li");

    for (let i = 0; i < li.length; i++){
        let a = li[i].getElementsByTagName("a")[0];
        let txtValue = a.textContent || a.innerText;
        if(txtValue.toUpperCase().indexOf(filter) > -1){
            li[i].style.display = "";
        }else{
            li[i].style.display = "none";
        }
    }

    // highlight the searching text
    let text = document.getElementById("mySearch").value;
    let query = new RegExp("(\\b" + text + "\\b)", "gim");
    let e = document.getElementById("myUL").innerHTML;
    let hiNew = e.replace(/(<span>|<\/span>)/img, "");
    document.getElementById("myUL").innerHTML = hiNew;
    let newHi = hiNew.replace(query, "<span>$1</span>");
    document.getElementById("myUL").innerHTML = newHi; 
}

function getDisplay() {
  let getText = document.getElementsByTagName("a").value;
  document.getSelection.innerHTML = getText;
}
// random images after refresh the pages
function imgRand() {
    let images =['./img/pinkRabbit.jpg', './img/br.jpg', './img/work.jpg', './img/giphy.jpg'];
    let numRand = Math.floor(Math.random()*images.length);
    document.getElementById("imgGo").src = images[numRand] + "";
}
imgRand();
window.setInterval("imgRand()", 3000);